var MeterCanvas = document.getElementById("MeterCanvas"),
    ctx     = MeterCanvas.getContext('2d');

    MeterCanvas.width = MeterCanvas.offsetWidth;
    MeterCanvas.height = MeterCanvas.offsetHeight;

    MeterCenterX = MeterCanvas.width/2;
    MeterCenterY = MeterCanvas.height - 10;
    MeterRadiusOut = MeterCanvas.width/2;
    MeterRadiusIn = MeterRadiusOut/2 + 10;
    ArrowRadius = MeterRadiusIn-20;

    SkillsSum = 500;
    SkillsCount = 1;
    SkillsValue = 0;
    SkillsMed  = 0;

    ArrowAngle = 0;

    SkillsList = "";

function DrawMeter() {
    ctx.beginPath();
    ctx.fillStyle = "rgb(255,200,21)";
    ctx.arc(MeterCenterX, MeterCenterY, MeterRadiusIn, Math.PI, 62/48 * Math.PI, false);
    ctx.arc(MeterCenterX, MeterCenterY, MeterRadiusOut, 62/48 * Math.PI, Math.PI, true);
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "rgb(163,205,59)";
    ctx.arc(MeterCenterX, MeterCenterY, MeterRadiusIn, 132/100 * Math.PI, 168/100 * Math.PI, false);
    ctx.arc(MeterCenterX, MeterCenterY, MeterRadiusOut, 169/100 * Math.PI, 131/100 * Math.PI, true);
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "rgb(0,147,215)";
    ctx.arc(MeterCenterX, MeterCenterY, MeterRadiusIn, 82/48 * Math.PI, 96/48 * Math.PI, false);
    ctx.arc(MeterCenterX, MeterCenterY, MeterRadiusOut, 96/48 * Math.PI, 82/48 * Math.PI, true);
    ctx.fill();
}

function DrawArrow() {
    ctx.clearRect(MeterCenterX-ArrowRadius,MeterCenterY-ArrowRadius+10,ArrowRadius*2,ArrowRadius+10);
    ctx.clearRect(MeterCenterX-ArrowRadius+15,MeterCenterY-ArrowRadius,ArrowRadius*2-30,ArrowRadius);

    ctx.beginPath();
    ctx.fillStyle = "rgb(62,61,64)";
    ctx.arc(MeterCenterX, MeterCenterY, 10, 0, 2*Math.PI, true);
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "rgb(62,61,64)";
    ctx.moveTo(MeterCenterX-10*Math.cos(Math.PI/2-ArrowAngle),MeterCenterY+10*Math.sin(Math.PI/2-ArrowAngle));
    ctx.lineTo(MeterCenterX-ArrowRadius*Math.cos(ArrowAngle),MeterCenterY-ArrowRadius*Math.sin(ArrowAngle));
    ctx.lineTo(MeterCenterX+10*Math.cos(Math.PI/2-ArrowAngle),MeterCenterY-10*Math.sin(Math.PI/2-+ArrowAngle));
    ctx.fill();
}

function init(){
    setInterval( function () {
        SkillsSum = 0;
        SkillsCount = 0;
        SkillsList = "";
        for (var CheckboxArray = document.querySelectorAll('input[type=checkbox]'), j = 0, J = CheckboxArray.length; j < J; j++){
            if (CheckboxArray [j].checked){
                SkillsList = SkillsList+CheckboxArray [j].name+", ";
                SkillsSum = SkillsSum + Number(CheckboxArray [j].value);
                SkillsCount++;
            }
        }
        if(SkillsCount>0){
            SkillsMed = Math.floor(SkillsSum/SkillsCount);

        }else {
            SkillsMed = 0;
            SkillsCount = 1;
        }

        if(SkillsValue<SkillsMed) {
            SkillsValue++;
        }else if(SkillsValue>SkillsMed){
            SkillsValue--;
        }

        document.getElementById('skills-table').innerHTML = SkillsList.slice(0,-2);

        ArrowAngle = SkillsValue/100*Math.PI;
        if(SkillsValue*SkillsCount != document.getElementById('digital-table').innerHTML){
            document.getElementById('digital-table').innerHTML = SkillsValue*SkillsCount;
        };
        DrawArrow();
    }, 25);
}

window.addEventListener('DOMContentLoaded', function () {
    DrawMeter();
    MeterCanvas = document.querySelector('#MeterCanvas');
    ctx = MeterCanvas.getContext('2d');
    DrawArrow();
    if ( ctx ) {
        init();
    }
}, false);


